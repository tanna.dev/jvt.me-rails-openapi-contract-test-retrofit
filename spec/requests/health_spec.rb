require 'rails_helper'

RSpec.describe HealthController, type: :request do
  describe 'GET /health' do
    it 'returns 200 OK' do
      get health_index_url

      expect(response).to have_http_status(:ok)
    end

    it 'returns response body' do
      expected = {
        status: :ok
      }

      get health_index_url

      expect(response.media_type).to eq('application/json')
      expect(response.body).to eq(expected.to_json)
    end
  end
end
