class City < ApplicationRecord
  validates_length_of :name, minimum: 1, allow_blank: false
end
