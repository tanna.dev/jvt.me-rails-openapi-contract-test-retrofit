require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module CitiesApi
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")

    # Only loads a smaller set of middleware suitable for API only apps.
    # Middleware like session, flash, cookies can be added back manually.
    # Skip views, helpers and assets when generating a new resource.
    config.api_only = true

    config.middleware.use Committee::Middleware::RequestValidation,
                          schema_path: 'docs/openapi.yml',
                          raise: !Rails.env.production?,
                          ignore_error: true,
                          error_handler: lambda { |error, _|
                                           Rails.logger.warn("OpenAPI Validation failed on the request: #{error.message}")
                                         }

    config.middleware.use Committee::Middleware::ResponseValidation,
                          schema_path: 'docs/openapi.yml',
                          raise: !Rails.env.production?,
                          ignore_error: true,
                          error_handler: lambda { |error, _|
                                           Rails.logger.warn("OpenAPI Validation failed on the request: #{error.message}")
                                         }
  end
end
